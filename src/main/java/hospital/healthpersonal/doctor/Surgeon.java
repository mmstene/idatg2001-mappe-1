package hospital.healthpersonal.doctor;

import hospital.people.Patient;

/**
 * The type hospital.healthpersonal.doctor.Surgeon.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Surgeon extends Doctor{
    /**
     * Instantiates a new hospital.healthpersonal.doctor.Surgeon.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis){

    }
}
