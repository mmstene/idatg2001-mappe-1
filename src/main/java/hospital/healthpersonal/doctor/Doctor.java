package hospital.healthpersonal.doctor;

import hospital.people.Employee;
import hospital.people.Patient;

/**
 * The type hospital.healthpersonal.doctor.Doctor.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public abstract class Doctor extends Employee {

    /**
     * Adding a default constructor so that this class can be inherited from.
     */
    protected Doctor() {
    }
    
    /**
     * Instantiates a new hospital.healthpersonal.doctor.Doctor.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets diagnosis.
     *
     * @param patient   the patient
     * @param diagnosis the diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
