package hospital.healthpersonal.doctor;

import hospital.people.Patient;

/**
 * The type General practitioner.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class GeneralPractitioner extends Doctor{


    /**
     * Instantiates a new General practitioner.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String diagnosis){

    }

}
