package hospital.healthpersonal;

import hospital.people.Employee;

/**
 * The type hospital.healthpersonal.Nurse.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Nurse extends Employee {
    /**
     * Instantiates a new hospital.healthpersonal.Nurse.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
