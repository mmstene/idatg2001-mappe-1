package hospital;

import hospital.exception.RemoveException;

/**
 * The type Hospital application. Used as a main class.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class HospitalApplication {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        Hospital hospital = new Hospital("Kristiansund Sykehus");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department department = hospital.getDepartments().get("Akutten");

        try {
            // Should succeed
            department.remove(department.getEmployeeArrayList().get(0));
            // Should fail
            department.remove(department.getPatientArrayList().get(5));
        } catch (RemoveException | IndexOutOfBoundsException e) {
            System.err.println("An error has occured " + e.getMessage());
        }
    }
}
