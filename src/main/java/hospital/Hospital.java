package hospital;

import java.util.HashMap;

/**
 * The type hospital.Hospital.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Hospital {
    // Initializing variables
    private final String hospitalName;
    /*
    * Using a HashMap so that a department can easily be found, and no other department in the same
    * hospital can be named the same thing, no extra method needed to make this a thing.
    * But we have a equals and hashcode that does this, so could have used another kind of list also. But I like
    * the options given to us using a HashMap in this instance.
     */
    private HashMap<String, Department> departmentHashMap;

    /**
     * Instantiates a new Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departmentHashMap = new HashMap<>();
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public HashMap<String, Department> getDepartments() {
        return departmentHashMap;
    }

    /**
     * Add department to the HashMap using the String as key.
     *
     * @param nameOfDepartment the name of department
     * @param department       the department
     */
    public void addDepartment(String nameOfDepartment, Department department) {
        departmentHashMap.put(nameOfDepartment, department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departmentHashMap=" + departmentHashMap +
                '}';
    }
}
