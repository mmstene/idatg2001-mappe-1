package hospital.people;

import hospital.Diagnosable;

/**
 * The type hospital.people.Patient.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis;

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Instantiates a new Patient
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        /*
         * Chose public instead of protected as shown on the class diagram.
         * Reason being that the test class could not get the info needed after packaging.
         * There is also no reason for it being protected as employees are not protected either.
         */
        super(firstName, lastName, socialSecurityNumber);
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient " + super.toString() +
                "diagnosis='" + diagnosis + '\'' +
                '}' + "\n";
    }
}
