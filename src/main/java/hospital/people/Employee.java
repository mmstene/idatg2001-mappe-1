package hospital.people;

/**
 * The type hospital.people.Employee.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Employee extends Person{

    /**
     * Instantiates a new hospital.people.Employee.
     */
    public Employee() {
    }

    /**
     * Instantiates a new hospital.people.Employee.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Employee " + super.toString();
    }
}
