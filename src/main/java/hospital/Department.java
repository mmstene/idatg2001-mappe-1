package hospital;

import hospital.exception.RemoveException;
import hospital.people.Employee;
import hospital.people.Patient;
import hospital.people.Person;

import java.util.ArrayList;
import java.util.Objects;

/**
 * The type hospital.Department.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class Department {
    // Initializing some variables
    private String departmentName;
    private ArrayList<Employee> employeeArrayList;
    private ArrayList<Patient> patientArrayList;

    /**
     * Instantiates a new hospital.Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
        employeeArrayList = new ArrayList<>();
        patientArrayList = new ArrayList<>();
    }

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets employee array list.
     *
     * @return the employee array list
     */
    public ArrayList<Employee> getEmployeeArrayList() {
        return employeeArrayList;
    }

    /**
     * Gets patient array list.
     *
     * @return the patient array list
     */
    public ArrayList<Patient> getPatientArrayList() {
        return patientArrayList;
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee) {
        employeeArrayList.add(employee);
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient) {
        patientArrayList.add(patient);
    }

    /**
     * A method for removing a hospital.people.Person from the right list.
     * First checking if the lists of both employees and patients is empty.
     * Then using the "instanceOf" to check if the person used as the parameter is an hospital.people.Employee or hospital.people.Patient.
     * Then checking to see if the person is in the list, then removing said person.
     * If the person does not exist, I throw an RemoveException which will show the user a fulfilling message.
     *
     * @param person the person
     * @throws RemoveException if needed
     */
    public void remove(Person person) throws RemoveException {
        if (patientArrayList.isEmpty() && employeeArrayList.isEmpty()) {
            throw new RemoveException("The patients or the employees doesn't exist.");
        } else if (person instanceof Employee) {
            if (employeeArrayList.contains(person)) {
                employeeArrayList.remove(person);
            } else {
                throw new RemoveException("The list of employees was empty or could not be found");
            }
        } else if (person instanceof Patient) {
            if (patientArrayList.contains(person)) {
                patientArrayList.remove(person);

            } else {
                throw new RemoveException("The list of patients was empty or could not be found");
            }
        }
    }

    /**
     * Using equals and hashcode to check if a department name is equal to one another.
     *
     * @param o the object
     * @return a department name
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }
}
