package hospital.exception;

/**
 * The type Remove hospital.exception.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public class RemoveException extends Exception {
    /**
     * Instantiates a new Remove hospital.exception.
     *
     * @param exceptionMessage the error message the user gets.
     */
    public RemoveException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
