package hospital;

/**
 * The interface Diagnosable.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public interface Diagnosable{
    /**
     * Sets diagnosis.
     *
     * @param diagnosis the diagnosis
     */
    void setDiagnosis(String diagnosis);
}
