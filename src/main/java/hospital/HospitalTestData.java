package hospital;

import hospital.healthpersonal.doctor.GeneralPractitioner;
import hospital.healthpersonal.Nurse;
import hospital.healthpersonal.doctor.Surgeon;
import hospital.people.Employee;
import hospital.people.Patient;

/**
 * The type Hospital test data.
 * @author Martin Stene
 * @version 1.14.0.2
 */
public final class HospitalTestData {
    private HospitalTestData() {
        //not called
    }

    /**
     * Fills a hospital with departments with employees and patients.
     * @param hospital the hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        //Add some departments, employees and patients
        Department emergency = new Department("Akutten");
        emergency.addEmployee(new Employee("OddEven", "Primtallet", "17113526696"));
        emergency.addEmployee(new Employee("Huppasahn", "DelFinito", "01078988011"));
        emergency.addEmployee(new Employee("Rigmor", "Mortis", "15092235464"));
        emergency.addEmployee(new GeneralPractitioner("Inco", "Gnito", "16091570754"));
        emergency.addEmployee(new Surgeon("Inco", "Gnito", "19111639577"));
        emergency.addEmployee(new Nurse("Nina", "Teknologi", "17050618269"));
        emergency.addEmployee(new Nurse("Ove", "Ralt", "22095738382"));
        emergency.addPatient(new Patient("Inga", "Lykke", "21091479637"));
        emergency.addPatient(new Patient("Ulrik", "Smål", "08076414999"));
        hospital.addDepartment("Akutten", emergency);
        Department childrenPolyclinic = new Department("Barnpoliklinikk");
        childrenPolyclinic.addEmployee(new Employee("Salti", "Kaffen", "23114532347"));
        childrenPolyclinic.addEmployee(new Employee("NidelV.", "Elvefølger", "17048913946"));
        childrenPolyclinic.addEmployee(new Employee("Anton", "Nym", "03117838799"));
        childrenPolyclinic.addEmployee(new GeneralPractitioner("Gene", "Sis", "21089038097"));
        childrenPolyclinic.addEmployee(new Surgeon("Nanna", "Na", "03115735402"));
        childrenPolyclinic.addEmployee(new Nurse("Nora", "Toriet", "09058920778"));
        childrenPolyclinic.addPatient(new Patient("Hans", "Omvar", "14115539583"));
        childrenPolyclinic.addPatient(new Patient("Laila", "La", "04075502880"));
        childrenPolyclinic.addPatient(new Patient("Jøran", "Drebli", "22045218221"));
        hospital.addDepartment("Barnpoliklinikk",childrenPolyclinic);
    }
}
