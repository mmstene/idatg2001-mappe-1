import hospital.*;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Department test. Testing if the remove method from the Department class actually works.
 * @author Martin Stene
 * @version 1.14.0.2
 */
class DepartmentTest {

    // Making a variable so that it can be used in all the methods
    private Department department;

    /**
    * Using a @BeforeEach to signalize jUnit to initialize these things before every
    * test method in this class.
     */
    @BeforeEach
    void init() {
        System.out.println("Initializing test");
        Hospital hospital = new Hospital("St. Olavs");
        HospitalTestData.fillRegisterWithTestData(hospital);
        department = hospital.getDepartments().get("Akutten");
    }

    @AfterEach
    void teardown() {
        System.out.println("Ended test, preparing for the next");
    }

    /**
     * Testing using the size of the list
     * Checking if the list is the same size after the remove method is used.
     * Doing this on each test.
     */
    @Test
    @DisplayName("Testing to see if a hospital.people.Employee is removed expecting negative outcome")
    void removeNegativeTestEmployee() throws RemoveException {
        int sizeBefore = department.getEmployeeArrayList().size();
        department.remove(department.getEmployeeArrayList().get(0));
        int sizeAfter = department.getEmployeeArrayList().size();

        fail("Employee is removed, it should fail when you compare the lists before and after.");
        assertEquals(sizeBefore, sizeAfter);
    }

    @Test
    @DisplayName("Testing to see if a hospital.people.Patient is removed expecting negative outcome")
    void removeNegativeTestPatient() throws RemoveException {
        int sizeBefore = department.getPatientArrayList().size();
        department.remove(department.getPatientArrayList().get(0));
        int sizeAfter = department.getPatientArrayList().size();

        fail("Patient is removed, it should fail when you compare the lists before and after.");
        assertEquals(sizeBefore, sizeAfter);
    }

    @Test
    @DisplayName("Testing to see if a hospital.people.Employee is removed correctly, positive outcome expected")
    void removePositiveTestEmployee() throws RemoveException {
        int sizeBefore = department.getEmployeeArrayList().size();
        department.remove(department.getEmployeeArrayList().get(0));
        int sizeAfter = department.getEmployeeArrayList().size();

        assertEquals(sizeAfter + 1, sizeBefore);
    }

    @Test
    @DisplayName("Testing to see if a hospital.people.Patient is removed correctly, positive outcome expected")
    void removePositiveTestPatient() throws RemoveException {
        int sizeBefore = department.getPatientArrayList().size();
        department.remove(department.getPatientArrayList().get(0));
        int sizeAfter = department.getPatientArrayList().size();

        assertEquals(sizeAfter + 1, sizeBefore);
    }
}